local S = minetest.get_translator("colorcubes")

local cc = {}
cc.colors = { "white", "yellow", "orange", "red", "redviolet", "magenta", "violet", "blue", "skyblue", "cyan", "aqua", "green", "lime", "pink", "brown", "dark_green", "light_gray", "dark_gray", "black" }
cc.dye_colors = {
	white = "unicolor_white", yellow = "unicolor_yellow", orange = "unicolor_orange", red = "unicolor_red", redviolet = "unicolor_red_violet", magenta = "unicolor_magenta", violet = "unicolor_violet", blue = "unicolor_blue", skyblue = "unicolor_sky_blue", cyan = "unicolor_cyan", aqua = "unicolor_aqua", green = "unicolor_green", lime = "unicolor_lime", pink = "unicolor_light_red", brown = "unicolor_dark_orange", dark_green = "unicolor_dark_green", light_gray = "unicolor_grey", dark_gray = "unicolor_darkgrey", black = "unicolor_black" }
cc.human_colors = {
	single = {
		white = S("White basic color cube"),
		yellow = S("Yellow basic color cube"),
		orange = S("Orange basic color cube"),
		red = S("Red basic color cube"),
		redviolet = S("Red-violet basic color cube"),
		magenta = S("Magenta basic color cube"),
		violet = S("Violet basic color cube"),
		blue = S("Blue basic color cube"),
		skyblue = S("Skyblue basic color cube"),
		cyan = S("Cyan basic color cube"),
		aqua = S("Aqua basic color cube"),
		green = S("Green basic color cube"),
		lime = S("Lime basic color cube"),
		pink = S("Pink basic color cube"),
		brown = S("Brown basic color cube"),
		dark_green = S("Dark green basic color cube"),
		light_gray = S("Light gray basic color cube"),
		dark_gray = S("Dark gray basic color cube"),
		black = S("Black basic color cube"),
	},
	window = {
		white = S("White window color cube"),
		yellow = S("Yellow window color cube"),
		orange = S("Orange window color cube"),
		red = S("Red window color cube"),
		redviolet = S("Red-violet window color cube"),
		magenta = S("Magenta window color cube"),
		violet = S("Violet window color cube"),
		blue = S("Blue window color cube"),
		skyblue = S("Skyblue window color cube"),
		cyan = S("Cyan window color cube"),
		aqua = S("Aqua window color cube"),
		green = S("Green window color cube"),
		lime = S("Lime window color cube"),
		pink = S("Pink window color cube"),
		brown = S("Brown window color cube"),
		dark_green = S("Dark green window color cube"),
		light_gray = S("Light gray window color cube"),
		dark_gray = S("Dark gray window color cube"),
		black = S("Black window color cube"),
	},
	tiled = {
		white = S("White tiled color cube"),
		yellow = S("Yellow tiled color cube"),
		orange = S("Orange tiled color cube"),
		red = S("Red tiled color cube"),
		redviolet = S("Red-violet tiled color cube"),
		magenta = S("Magenta tiled color cube"),
		violet = S("Violet tiled color cube"),
		blue = S("Blue tiled color cube"),
		skyblue = S("Skyblue tiled color cube"),
		cyan = S("Cyan tiled color cube"),
		aqua = S("Aqua tiled color cube"),
		green = S("Green tiled color cube"),
		lime = S("Lime tiled color cube"),
		pink = S("Pink tiled color cube"),
		brown = S("Brown tiled color cube"),
		dark_green = S("Dark green tiled color cube"),
		light_gray = S("Light gray tiled color cube"),
		dark_gray = S("Dark gray tiled color cube"),
		black = S("Black tiled color cube"),
	},
	inward = {
		white = S("White concentric color cube"),
		yellow = S("Yellow concentric color cube"),
		orange = S("Orange concentric color cube"),
		red = S("Red concentric color cube"),
		redviolet = S("Red-violet concentric color cube"),
		magenta = S("Magenta concentric color cube"),
		violet = S("Violet concentric color cube"),
		blue = S("Blue concentric color cube"),
		skyblue = S("Skyblue concentric color cube"),
		cyan = S("Cyan concentric color cube"),
		aqua = S("Aqua concentric color cube"),
		green = S("Green concentric color cube"),
		lime = S("Lime concentric color cube"),
		pink = S("Pink concentric color cube"),
		brown = S("Brown concentric color cube"),
		dark_green = S("Dark green concentric color cube"),
		light_gray = S("Light gray concentric color cube"),
		dark_gray = S("Dark gray concentric color cube"),
		black = S("Black concentric color cube"),
	},
	light = {
		white = S("White light color cube"),
		yellow = S("Yellow light color cube"),
		orange = S("Orange light color cube"),
		red = S("Red light color cube"),
		redviolet = S("Red-violet light color cube"),
		magenta = S("Magenta light color cube"),
		violet = S("Violet light color cube"),
		blue = S("Blue light color cube"),
		skyblue = S("Skyblue light color cube"),
		cyan = S("Cyan light color cube"),
		aqua = S("Aqua light color cube"),
		green = S("Green light color cube"),
		lime = S("Lime light color cube"),
	}
}

local light_level = 14

local soundmaker = function (mod)
	if mod == nil then mod = 0 end

	local scrub = function(value)
		return math.min(1, math.max(value, 0))
	end

	return { footstep = { name = "colorcubes_block_footstep", gain = scrub(0.25 + mod) },
			dig = { name = "colorcubes_block_dig", gain = scrub(1) },
			dug = { name = "colorcubes_block_dug", gain = scrub(0.7 + mod) },
			place = { name = "colorcubes_block_place", gain = 0.5 }, }
end

--[[
Color Cubes groups for better identification:
colorcube=1: this item is a color cube
colorcube_single=1: basic color cube
colorcube_window=1: window color cube
colorcube_tiled=1: tiled color cube (1 color)
colorcube_tiled=2: tiled color cube (2 colors)
colorcube_inward=1: concentric color cube
colorcube_light=1: light color cube
]]

for i=1,#cc.colors do
	local c = cc.colors[i]
	local d = cc.dye_colors[c]
	-- Blocks with a simple square pattern
	local nodedef1 = {
		description = cc.human_colors.single[c],
		tiles = { "colorcubes_1_"..c..".png" },
		groups = { dig_immediate = 2, colorcube = 1, colorcube_single = 1 },
		sounds = soundmaker(0),
	}
	minetest.register_node("colorcubes:"..c.."_single", nodedef1)
	-- Windows
	local nodedef_window = {
		description = cc.human_colors.window[c],
		tiles = { "colorcubes_window_"..c..".png" },
		groups = { dig_immediate = 2, colorcube = 1, colorcube_window = 1 },
		use_texture_alpha = true,
		drawtype = "glasslike",
		sunlight_propagates = true,
		paramtype = "light",
		sounds = soundmaker(-0.1),
	}
	minetest.register_node("colorcubes:"..c.."_window", nodedef_window)
	-- Blocks with a tile pattern (4 tiles)
	local nodedef4 = {
		description = cc.human_colors.tiled[c],
		tiles = { "colorcubes_4_"..c..".png" },
		groups = { dig_immediate = 2, colorcube = 1, colorcube_tiled = 1 },
		sounds = soundmaker(0.1),
	}
	minetest.register_node("colorcubes:"..c.."_tiled", nodedef4)
	-- Blocks with a concentric square pattern
	local nodedef_inward = {
		description = cc.human_colors.inward[c],
		tiles = { "colorcubes_inward_"..c..".png" },
		groups = { dig_immediate = 2, colorcube = 1, colorcube_inward = 1},
		sounds = soundmaker(0.15),
	}
	minetest.register_node("colorcubes:"..c.."_inward", nodedef_inward)
	-- Lamps in different brightness levels
	if light_level > 1 then
		local nodedef_light = {
			description = cc.human_colors.light[c],
			tiles = { "colorcubes_light_"..c..".png" },
			groups = { dig_immediate = 2, colorcube = 1, colorcube_light = 1 },
			sounds = soundmaker(-0.05),
			light_source = light_level,
		}
		minetest.register_node("colorcubes:"..c.."_light", nodedef_light)
		if(minetest.get_modpath("paint_roller") ~= nil) then
			paint_roller.register_one("colorcubes:"..c.."_light", d, "light color cubes" )
		end
		light_level = light_level - 1
	end

	--[[ If the paint_roller mod is installed, register the nodes to it ]]
	if(minetest.get_modpath("paint_roller") ~= nil) then
		paint_roller.register_one("colorcubes:"..c.."_single", d, "basic color cubes" )
		paint_roller.register_one("colorcubes:"..c.."_tiled", d, "tiled color cubes" )
		paint_roller.register_one("colorcubes:"..c.."_inward", d, "concentric color cubes" )
		paint_roller.register_one("colorcubes:"..c.."_window", d, "window color cubes" )
	end

	-- Register various crafting recipes for convenience
	minetest.register_craft({
		output = "colorcubes:"..c.."_tiled",
		recipe = {
			{"colorcubes:"..c.."_single", "colorcubes:"..c.."_single" },
			{"colorcubes:"..c.."_single", "colorcubes:"..c.."_single" },
		},
	})
	minetest.register_craft({
		output = "colorcubes:"..c.."_inward",
		recipe = {
			{"colorcubes:"..c.."_single", "colorcubes:"..c.."_single", "colorcubes:"..c.."_single" },
			{"colorcubes:"..c.."_single", "", "colorcubes:"..c.."_single" },
			{"colorcubes:"..c.."_single", "colorcubes:"..c.."_single", "colorcubes:"..c.."_single" },
		},
	})
	minetest.register_craft({
		output = "colorcubes:"..c.."_single 8",
		recipe = {
			{"colorcubes:"..c.."_inward" }
		},
	})
	minetest.register_craft({
		output = "colorcubes:"..c.."_single 4",
		recipe = {
			{"colorcubes:"..c.."_tiled" }
		},
	})

	-- Color recipes (dye + color cube)
	minetest.register_craft({
		type = "shapeless",
		output = "colorcubes:"..c.."_single",
		recipe = { "group:dye,"..d, "group:colorcube_single" },
	})
	minetest.register_craft({
		type = "shapeless",
		output = "colorcubes:"..c.."_tiled",
		recipe = { "group:dye,"..d, "group:colorcube_tiled" },
	})
	minetest.register_craft({
		type = "shapeless",
		output = "colorcubes:"..c.."_inward",
		recipe = { "group:dye,"..d, "group:colorcube_inward" },
	})
	minetest.register_craft({
		type = "shapeless",
		output = "colorcubes:"..c.."_light",
		recipe = { "group:dye,"..d, "group:colorcube_light" },
	})
	minetest.register_craft({
		type = "shapeless",
		output = "colorcubes:"..c.."_window",
		recipe = { "group:dye,"..d, "group:colorcube_window" },
	})

end

--[[ Tiled blocks with 2 different colors (usuall complementary colors) ]]

local complementary = { { "yellow", "blue"}, {"aqua", "redviolet"}, {"red", "cyan"}, {"light_gray", "dark_gray"}, {"green", "magenta"}, {"orange", "skyblue"}, {"lime", "violet"}, {"black", "white"}, {"orange", "brown"} }
local complementary_names = {
	S("Yellow/blue tiled color cube"),
	S("Aqua/red-violet tiled color cube"),
	S("Red/cyan tiled color cube"),
	S("Light/dark gray tiled color cube"),
	S("Green/magenta tiled color cube"),
	S("Orange/skyblue tiled color cube"),
	S("Lime/violet tiled color cube"),
	S("Black/white tiled color cube"),
	S("Orange/brown tiled color cube"),
}

for i=1,#complementary do
	local c1, c2
	c1 = complementary[i][1]
	c2 = complementary[i][2]
	local nodeid = "colorcubes:"..c1.."_"..c2.."_tiled"
	local tex = "colorcubes_4c_"..c1.."_"..c2..".png"
	local texR90 = tex .. "^[transformR90"
	minetest.register_node(nodeid, {
		description = complementary_names[i],
		tiles = { tex, tex, tex, tex, texR90, texR90 },
		groups = { dig_immediate = 2, colorcube = 1, colorcube_tiled = 2 },
		sounds = soundmaker(0.1),

	})
	minetest.register_craft({
		output = nodeid,
		recipe = {
			{"colorcubes:"..c1.."_single", "colorcubes:"..c2.."_single" },
			{"colorcubes:"..c2.."_single", "colorcubes:"..c1.."_single" },
		},
	})
	minetest.register_craft({
		output = nodeid,
		recipe = {
			{"colorcubes:"..c2.."_single", "colorcubes:"..c1.."_single" },
			{"colorcubes:"..c1.."_single", "colorcubes:"..c2.."_single" },
		},
	})

	--[[ This register the block to the paint roller mod. Note that this block can
	only be painted, but it can not be reversed; it will not be possible to paint a
	tiled block so that it becomes a 2-color block. Thus, we use minor hack by using
	a fake dye group called “none” to make sure there is no dye to turn a tiled block
	into a 2-color tiled block. ]]
	if(minetest.get_modpath("paint_roller") ~= nil) then
		paint_roller.register_one(nodeid, "none", "tiled color cubes" )
	end
end
